package com.cschool.example;

public class LambdaExample {

    public static void main(String[] args) {

        SumImpl sumImpl = new SumImpl();


        Sum sumInstance = new Sum() {

            @Override
            public int calculate(int a, int b) {
                return 0;
            }
        };

        System.out.println(sumImpl.calculate(2,6));
        System.out.println(sumInstance.calculate(2,6));

    }
}
