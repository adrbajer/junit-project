package com.cschool.example;

public class SimpleCalculator {

    public static int add (int a, int b){
        return a + b;
    }

    public static int multiply (int a, int b){
        return a * b;
    }

    public static double divide (double a, double b){
        if (b == 0){
            throw new IllegalArgumentException("Błąd! Dzielenie przez 0.");
        }
        return a / b;
    }

    public static void main(String[] args) {
        System.out.println("Simple Calculator");

    }
}
