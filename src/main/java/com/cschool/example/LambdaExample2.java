package com.cschool.example;

public class LambdaExample2 {

    public static void main(String[] args) {

        Sum sumInstance = (a, b) -> a * b + 10;

        System.out.println(sumInstance.calculate(2,3));


        SumString sumStringInstance = (str) -> str + "abc";
        System.out.println(sumStringInstance.addStrings("Dom"));

        SumString sumStringInstance2 = (str) -> str + "abc".toUpperCase();
        System.out.println(sumStringInstance2.addStrings("Dom"));

        SumString sumStringInstance3 = str -> {
            String upperCase = str.toUpperCase();
            return str.concat(upperCase);
        };

        PowerOn powerOn = () -> {
            System.out.println(SimpleCalculator.add(3, 5));
        };

        powerOn.activate();




    }
}
