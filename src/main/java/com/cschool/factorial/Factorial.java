package com.cschool.factorial;

import java.math.BigInteger;

public class Factorial {

    public static long calculateFactorial(int userNumber) {
        long factorial = 1;
        for (int i = 1; i <= userNumber; i++) {
            factorial = factorial * i;
        }
        return factorial;
    }

    public static BigInteger countFactorialForSelectedNumber2(int inputNumber){
        BigInteger factorial = new BigInteger("1");
        for (int i = 1; i <= inputNumber; i++) {
            factorial = factorial.multiply(BigInteger.valueOf(i));
        }
        return factorial;
    }

    public static void main(String[] args) {
        System.out.println(countFactorialForSelectedNumber2(10));
    }

}