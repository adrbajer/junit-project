package com.cschool.currency_exchange;

import org.decimal4j.util.DoubleRounder;

public class CurrencyExchange {

    private String name;
    private double exchangeRate;

    private final DoubleRounder doubleRounder = new DoubleRounder(3);

    public CurrencyExchange(String name, double exchangeRate) {
        this.name = name;
        this.exchangeRate = exchangeRate;
    }


    public double convertEuroToZl(double euroAmount){

        return doubleRounder.round(euroAmount * exchangeRate);
    }

    public double convertZlToEuro(double zlAmount){
        return doubleRounder.round(zlAmount / exchangeRate);
    }


}
