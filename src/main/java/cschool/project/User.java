package cschool.project;

public class User {

    private String name;
    private int age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name.length() > 5){
            this.name = name;
        }else{
            throw new IllegalArgumentException("User name should be longer than 5 characters");
        }
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        if (age >= 18){
            this.age = age;
        }else{
            throw new IllegalArgumentException("User is younger than 18");
        }
    }
}
