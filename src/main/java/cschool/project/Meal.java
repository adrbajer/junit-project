package cschool.project;

import java.util.Objects;

public class Meal {

    private int price;
    private String name;

    public Meal(int price) {
        this.price = price;
    }

    public Meal(int price, String name) {
        this.price = price;
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public int getDiscountPrice(int discount){
        if (discount > price){
            throw new IllegalArgumentException("Discount is bigger than price");
        }
        return this.price - discount;
    }

    public static void main(String[] args) {

        Meal meal_10 = new Meal(10);
        System.out.println(meal_10.getDiscountPrice(4));

        Meal meal = new Meal(10);
        Meal meal2 = new Meal(10);
        Meal meal3 = new Meal(20);

        System.out.println(meal.equals(meal2));
        System.out.println(meal.equals(meal3));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Meal meal = (Meal) o;
        return price == meal.price &&
                Objects.equals(name, meal.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(price, name);
    }
}
