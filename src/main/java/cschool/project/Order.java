package cschool.project;

import java.util.ArrayList;
import java.util.List;

public class Order {

    private List<Meal> meals = new ArrayList<>();

    public void addMealToOrder(Meal mealToAdd) {
        meals.add(mealToAdd);
    }

    public void removeMealFromOrder(Meal meal) {
        meals.remove(meal);
    }

    public List<Meal> getMeals() {
        return meals;
    }

    public void addMealToOrder(List<Meal> mealList1) {
        meals.addAll(mealList1);
    }
}
