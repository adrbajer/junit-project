package com.cschool.factorial;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigInteger;

import static org.junit.jupiter.api.Assertions.*;

class FactorialTest {



    @Test
    void countFactorialForSelectedNumber2ForInputNumber5() {
        Assertions.assertEquals(BigInteger.valueOf(120),Factorial.countFactorialForSelectedNumber2(5));
    }

    @Test
    void countFactorialForSelectedNumber2ForInputNumber10() {
        Assertions.assertEquals(BigInteger.valueOf(3628800),Factorial.countFactorialForSelectedNumber2(10));
    }

    @Test
    void countFactorialForSelectedNumber2ForInputNumber20() {
        Assertions.assertEquals(new BigInteger("2432902008176640000"),Factorial.countFactorialForSelectedNumber2(20));
    }
}