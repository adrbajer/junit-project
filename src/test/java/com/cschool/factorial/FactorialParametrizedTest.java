package com.cschool.factorial;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.math.BigInteger;
import java.sql.Statement;
import java.util.stream.Stream;

public class FactorialParametrizedTest {

    @ParameterizedTest
    @MethodSource("factorialDataProvider")
    void calculateFactorial(int factorial, int inputNumber){
        Assertions.assertEquals(factorial, Factorial.calculateFactorial(inputNumber));
    }

    @ParameterizedTest
    @MethodSource("factorialBigIntegerProvider")
    void countFactorialForSelectedNumber2(BigInteger factorial, int inputNumber){
        Assertions.assertEquals(factorial, Factorial.countFactorialForSelectedNumber2(inputNumber));
    }




    private static Stream factorialDataProvider(){

        return Stream.of(
                Arguments.of(120, 5),
                Arguments.of(40320, 8),
                Arguments.of(3628800, 10)
        );
    }


    private static Stream factorialBigIntegerProvider(){

        return Stream.of(
                Arguments.of(new BigInteger("120"), 5),
                Arguments.of(new BigInteger("3628800"), 10),
                Arguments.of(new BigInteger("2432902008176640000"), 20)

        );
    }

}
