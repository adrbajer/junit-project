package com.cschool.currency_exchange;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class CurrencyExchangeTest {

    CurrencyExchange currencyExchange;

    @BeforeEach
    void setUp(){
        currencyExchange = new CurrencyExchange("TestCurrencyExchange", 4.45);
    }

    @Test
    void exchangeEuroShouldReturnCorrectZlValue() {
        //given

        //when

        //then
        Assertions.assertEquals(17.4, currencyExchange.convertEuroToZl(4));
    }

    @Test
    void convertZlToEuro() {
        Assertions.assertEquals(112.479, currencyExchange.convertZlToEuro(500.533));
    }

//    @ParameterizedTest
//    @MethodSource
//
//
//    private static Stream testDataProvider(){
//
//        return Stream.of()
//
//
//    }
}