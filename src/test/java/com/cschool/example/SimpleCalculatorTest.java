package com.cschool.example;

import com.cschool.example.SimpleCalculator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class SimpleCalculatorTest {

    @Test
    void add() {
        Assertions.assertEquals(7, SimpleCalculator.add(3,4));
    }

    @Test
    void multiply() {
        Assertions.assertEquals(12,SimpleCalculator.multiply(3,4));
    }

    @Test
    void divide(){
        Assertions.assertEquals(5,SimpleCalculator.divide(5,1));
    }

    @Test
    void shouldThrowIllegalArgumentExceptionWhenDivisionBy0() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {SimpleCalculator.divide(5,0);});

    }
}