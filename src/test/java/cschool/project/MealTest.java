package cschool.project;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import static org.junit.jupiter.api.Assertions.*;

class MealTest {

    @Test
    void shouldReturnDiscountedPrice(){
        //given
        Meal meal = new Meal(10);

        //when
        int priceAfterDiscount = meal.getDiscountPrice(4);

        //then
        assertEquals(6, priceAfterDiscount);
    }

    @Test
    void ifDiscountBiggerThanPriceShouldThrowException(){
        //given
        Meal meal = new Meal(10);

        //when
        Executable lambda = () -> meal.getDiscountPrice(11);

        //then
        //1
//        Assertions.assertThrows(IllegalArgumentException.class, ()->{meal.getDiscountPrice(11);});

        //2
        assertThrows(IllegalArgumentException.class, lambda);

    }

    @Test
    void referencesToDifferentObjectShouldNotBeEqual(){
        //given
        Meal meal1 = new Meal (10);
        Meal meal2 = new Meal(20);

    assertNotSame(meal1,meal2);
    }

    @Test
    void twoMealsShouldBeEqualWhenPriceAndNameAreTheSame(){
        //given
        Meal meal1 = new Meal (10,"Burger");
        Meal meal2 = new Meal(10, "Burger");

        assertEquals(meal1,meal2);
    }

}