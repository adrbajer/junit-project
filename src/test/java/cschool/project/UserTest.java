package cschool.project;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.jupiter.api.Assertions.*;

class UserTest {

    @Test
    void newUserNameShouldBeNull() {
        User user1 = new User();

        assertThat(user1.getName(), nullValue());
    }

    @Test
    void userAgeIfSetAsLowerThan18ShouldThrowIllegalArgumentException() {
        User user1 = new User();

        Assertions.assertThrows(IllegalArgumentException.class, () -> user1.setAge(15));

    }

    @ParameterizedTest
    @ValueSource (strings = {"ada", "bob", "rob"})
    void userNameShorterThan5CharactersShouldThrowIllegalArgumentException(String string) {
        User user1 = new User();

        Assertions.assertThrows(IllegalArgumentException.class, () -> user1.setName(string));
    }

}