package cschool.project;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.*;

class AccountTest {

    @Test
    void newlyCreatedAccountShouldNotBeActive(){
        Account account = new Account();
        assertFalse(account.isActive(),
                "Check if new account is not active");

        assertThat(account.isActive(), equalTo(false));
        assertThat(account.isActive(), is(false));

    }

    @Test
    void newlyCreatedAccountShouldBeActiveAfterActivation(){
        Account account = new Account();
        account.activate();
        assertTrue(account.isActive());
    }

    @Test
    void newlyCreatedAccountShouldHaveDefaultDeliveryAddressEqualToNull(){
        //given
        Account account = new Account();

        //then

        assertNull(account.getDefaultDeliveryAddress());

        assertThat(account.getDefaultDeliveryAddress(), nullValue());
    }

    @Test
    void newlyCreatedAccountWithSetAddressShouldHaveDefaultDeliveryAddressNotEqualToNull(){
        //given
        Account account = new Account();

        //when
        account.setDefaultDeliveryAddress(new Address("Polna","68"));

        //then
        assertNotNull(account.getDefaultDeliveryAddress());
        assertThat(account.getDefaultDeliveryAddress(), notNullValue());
    }

    @Test
    void correctEmailAddressShouldNotBeEmpty() {

        //given
        Account account1 = new Account();
        //when
        account1.setEmailAddress("james@gmail.com");
        //then
        assertThat(account1.getEmailAddress(), notNullValue());
    }

    @Test
    void EmailAddressWithoutAtCharacterAndShorterThan10CharactersShouldThrowException() {
        //given
        Account account1 = new Account();

        //then
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            account1.setEmailAddress("jamesgmail.com");
        });
    }

    @ParameterizedTest
    @ValueSource (strings = {"james@wp.pl", "jk@gmail.com", "gtd@tlen.pl"})
    void correctEmailAddressShouldNotBeEmptyParameters(String string) {
        //given
        Account account1 = new Account();
        //when
        account1.setEmailAddress(string);
        //then
        assertThat(account1.getEmailAddress(), notNullValue());
    }
}